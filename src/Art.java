import java.net.Socket;

public class Art {
    private Socket clientSocket;
    private String articulo;

    public Art(String articulo) {
        this.articulo = articulo;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public void setClientSocket(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public String getArticulo() {
        return articulo;
    }
}
