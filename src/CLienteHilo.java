import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class CLienteHilo implements Runnable {
    Socket bpop;

    public CLienteHilo(Socket bpop) {
        this.bpop = bpop;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(bpop.getInputStream()));
            String message;

            while ((message = in.readLine()) != null) {
                System.out.println("User: " + message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
