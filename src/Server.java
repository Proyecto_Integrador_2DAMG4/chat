import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class Server {
    public static void main(String[] args) {
        ArrayList<Art> art = new ArrayList<>();

        if (args.length != 1) {
            System.err.println("Usage: java BatoiPop <port number>");
            System.exit(1);
        }

        int portNumber = Integer.parseInt(args[0]);

        try (ServerSocket serverSocket = new ServerSocket(portNumber)){
            while (true) {

                Hilo hilo = (new Hilo(serverSocket.accept(), art));

                Thread cliente = new Thread(hilo);
                cliente.start();

                for (int i = 0; i < art.size(); i++){
                    System.out.println(art.get(i).getArticulo());
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
