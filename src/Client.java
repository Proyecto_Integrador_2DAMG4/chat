import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

        try (Socket bpop = new Socket(hostName, portNumber); PrintWriter out = new PrintWriter(bpop.getOutputStream(), true)) {
            Thread cliente = new Thread(new CLienteHilo(bpop));
            cliente.start();
            System.out.print("Introduce la ID del articulo: ");

            while (true){
                Scanner scanner = new Scanner(System.in);
                String s = scanner.nextLine();

                out.println(s);
            }
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                    hostName);
            System.exit(1);
        }
    }
}
