import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Objects;

public class Hilo implements Runnable {
    Socket clientSocket;
    String articulo;
    ArrayList<Art> hilos;

    public Hilo(Socket accept, ArrayList<Art> hilos) {
        this.clientSocket = accept;
        this.hilos = hilos;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String inputLine, outputLine;

            String artName = in.readLine();
            Art art = new Art(artName);
            articulo = artName;
            art.setClientSocket(clientSocket);
            hilos.add(art);

            while ((inputLine = in.readLine()) != null){

                for (int i = 0; i < hilos.size(); i++) {
                    if (Objects.equals(hilos.get(i).getArticulo(), articulo)) {
                        if (hilos.get(i).getClientSocket() != this.getClientSocket()) {
                            PrintWriter out = new PrintWriter(hilos.get(i).getClientSocket().getOutputStream(), true);

                            out.println(inputLine);
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getClientSocket() {
        return clientSocket;
    }
}
